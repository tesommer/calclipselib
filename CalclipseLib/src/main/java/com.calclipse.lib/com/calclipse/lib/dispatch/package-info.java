/**
 * This package contains interfaces that are implemented in order to
 * allow one thread to execute code on another thread.
 * An example case is a worker thread that needs to access a UI control
 * that must be accessed from an event handling thread.
 */
package com.calclipse.lib.dispatch;
