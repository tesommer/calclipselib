package com.calclipse.lib.resource;

import static java.util.Objects.requireNonNull;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.function.Predicate;

import com.calclipse.lib.resource.internal.ArchivedResourceLocator;
import com.calclipse.lib.resource.internal.DirectoryResourceLocator;

/**
 * Locates a resource on a path component.
 * 
 * @see com.calclipse.lib.resource.Resources
 * 
 * @author Tone Sommerland
 */
@FunctionalInterface
public interface ResourceLocator
{
    /**
     * Returns a resource locator that searches directories
     * on the search path for a named resource.
     */
    public static ResourceLocator directories()
    {
        return DirectoryResourceLocator.INSTANCE;
    }
    
    /**
     * Returns a resource locator that searches archives
     * on the search path for a named resource.
     * When an archived resource is located,
     * it is loaded into memory.
     * @param searchable determines which path components to search
     * @see com.calclipse.lib.resource.SearchPaths#IS_JAR
     * @see com.calclipse.lib.resource.SearchPaths#IS_ZIP
     */
    public static ResourceLocator archives(
            final Predicate<? super File> searchable)
    {
        return new ArchivedResourceLocator(searchable);
    }
    
    /**
     * Attempts to locate the resource with the specified name
     * on the given path component.
     * @param name the resource name
     * @param pathComponent a component of the search path
     * @return {@code null} if the resource was not found
     */
    public abstract InputStream find(String name, File pathComponent)
            throws IOException;
    
    /**
     * Combine this resource locator with another.
     */
    public default ResourceLocator combinedWith(final ResourceLocator other)
    {
        requireNonNull(other, "other");
        return (name, pathComponent) ->
        {
            final InputStream in = find(name, pathComponent);
            if (in != null)
            {
                return in;
            }
            return other.find(name, pathComponent);
        };
    }

}
