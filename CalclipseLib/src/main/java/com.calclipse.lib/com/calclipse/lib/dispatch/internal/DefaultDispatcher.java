package com.calclipse.lib.dispatch.internal;

import static java.util.Objects.requireNonNull;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RunnableFuture;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

import com.calclipse.lib.dispatch.Dispatchable;
import com.calclipse.lib.dispatch.Dispatcher;
import com.calclipse.lib.dispatch.Disruption;

/**
 * The default dispatcher.
 * This dispatcher implements
 * {@link #consecutively(Dispatchable)},
 * and delegates {@link #isDestinationThread()}
 * and {@link #concurrently(Runnable)}
 * to functions given to this class' constructor.
 * 
 * @see com.calclipse.lib.dispatch.Dispatcher#depute(BooleanSupplier, Consumer)
 * 
 * @author Tone Sommerland
 */
public final class DefaultDispatcher implements Dispatcher
{
    private final BooleanSupplier isDestThread;
    private final Consumer<? super Runnable> concurrently;
    
    public DefaultDispatcher(
            final BooleanSupplier isDestThread,
            final Consumer<? super Runnable> concurrently)
    {
        this.isDestThread = requireNonNull(isDestThread, "isDestThread");
        this.concurrently = requireNonNull(concurrently, "concurrently");
    }

    @Override
    public boolean isDestinationThread()
    {
        return isDestThread.getAsBoolean();
    }

    @Override
    public void concurrently(final Runnable doRun)
    {
        concurrently.accept(requireNonNull(doRun, "doRun"));
    }

    @Override
    public <T> T consecutively(final Dispatchable<T> doRun) throws Disruption
    {
        requireNonNull(doRun, "doRun");
        if (isDestinationThread())
        {
            return doRun.execute();
        }
        final var future = new FutureTask<Result<T>>(asCallable(doRun));
        concurrently(future);
        return waitForResult(future);
    }

    private <T> T waitForResult(final RunnableFuture<Result<T>> future)
            throws Disruption
    {
        boolean interrupted = false;
        try
        {
            Result<T> result = null;
            do
            {
                try
                {
                    result = future.get();
                }
                catch (final InterruptedException ex)
                {
                    interrupted = true;
                }
                catch (final ExecutionException ex)
                {
                    throw new Disruption(ex);
                }
            }
            while (result == null);
            return result.get();
        }
        finally
        {
            if (interrupted)
            {
                Thread.currentThread().interrupt();
            }
        }
    }
    
    private static <T> Callable<Result<T>> asCallable(
            final Dispatchable<T> task)
    {
        return () ->
        {
            try
            {
                return Result.ofValue(task.execute());
            }
            catch (final Disruption ex)
            {
                return Result.ofDisruption(ex);
            }
        };
    }

}
