package com.calclipse.lib.paint;

import static java.util.Objects.requireNonNull;

import java.util.Map;
import java.util.Optional;

/**
 * Encapsulates a minimalistic set of font properties.
 * 
 * @author Tone Sommerland
 */
public final class Font
{
    /**
     * The name of the font property {@code "name"}.
     */
    public static final String KEY_NAME = "name";
    
    /**
     * The name of the font property {@code "size"}.
     */
    public static final String KEY_SIZE = "size";
    
    /**
     * The name of the font property {@code "weight"}.
     */
    public static final String KEY_WEIGHT = "weight";
    
    /**
     * The name of the font property {@code "style"}.
     */
    public static final String KEY_STYLE = "style";
    
    /**
     * The font-property value {@code "normal"}.
     */
    public static final String VALUE_NORMAL = "normal";
    
    /**
     * The font-property value {@code "bold"}.
     */
    public static final String VALUE_WEIGHT_BOLD = "bold";
    
    /**
     * The font-property value {@code "italic"}.
     */
    public static final String VALUE_STYLE_ITALIC = "italic";
    
    private final String name;
    private final int size;
    private final boolean bold;
    private final boolean italic;
    
    private Font(
            final String name,
            final int size,
            final boolean bold,
            final boolean italic)
    {
        this.name = requireNonNull(name, "name");
        this.size = size;
        this.bold = bold;
        this.italic = italic;
    }
    
    /**
     * Returns a font instance.
     * @param name the font name
     * @param size the font size
     * @param bold whether the font weight is bold or normal
     * @param italic whether the font style is italic or normal
     */
    public static Font of(
            final String name,
            final int size,
            final boolean bold,
            final boolean italic)
    {
        return new Font(name, size, bold, italic);
    }
    
    /**
     * Returns a font instance with normal weight and style.
     * @param name the font name
     * @param size the font size
     */
    public static Font plain(final String name, final int size)
    {
        return new Font(name, size, false, false);
    }
    
    /**
     * Returns a font instance with bold weight and normal style.
     * @param name the font name
     * @param size the font size
     */
    public static Font bold(final String name, final int size)
    {
        return new Font(name, size, true, false);
    }
    
    /**
     * Returns a font instance with normal weight and italic style.
     * @param name the font name
     * @param size the font size
     */
    public static Font italic(final String name, final int size)
    {
        return new Font(name, size, false, true);
    }
    
    /**
     * Returns a font instance with bold weight and italic style.
     * @param name the font name
     * @param size the font size
     */
    public static Font boldAndItalic(final String name, final int size)
    {
        return new Font(name, size, true, true);
    }

    /**
     * Returns a font instance with the given properties.
     * Returns empty if the name and/or size are absent,
     * or if the size is an invalid integer.
     * The returned font is bold if the weight property has the value
     * {@link #VALUE_WEIGHT_BOLD}.
     * The returned font is italic if the style property has the value
     * {@link #VALUE_STYLE_ITALIC}.
     * @param properties font properties
     */
    public static Optional<Font> fromProperties(
            final Map<String, String> properties)
    {
        if (
                   properties.containsKey(KEY_NAME)
                && properties.containsKey(KEY_SIZE))
        {
            try
            {
                return Optional.of(new Font(
                        properties.get(KEY_NAME),
                        Integer.parseInt(properties.get(KEY_SIZE)),
                        isBold(properties),
                        isItalic(properties)));
            }
            catch (final NumberFormatException ex)
            {
                return Optional.empty();
            }
        }
        else
        {
            return Optional.empty();
        }
    }

    private static boolean isBold(final Map<String, String> properties)
    {
        return VALUE_WEIGHT_BOLD.equals(properties.get(KEY_WEIGHT));
    }

    private static boolean isItalic(final Map<String, String> properties)
    {
        return VALUE_STYLE_ITALIC.equals(properties.get(KEY_STYLE));
    }

    /**
     * This font's properties.
     * @return a map containing the keys
     * {@link #KEY_NAME},
     * {@link #KEY_SIZE},
     * {@link #KEY_WEIGHT} and
     * {@link #KEY_STYLE}
     */
    public Map<String, String> properties()
    {
        return Map.of(
                KEY_NAME,   name,
                KEY_SIZE,   String.valueOf(size),
                KEY_WEIGHT, bold   ? VALUE_WEIGHT_BOLD  : VALUE_NORMAL,
                KEY_STYLE,  italic ? VALUE_STYLE_ITALIC : VALUE_NORMAL);
    }

    /**
     * The name of this font.
     */
    public String name()
    {
        return name;
    }

    /**
     * The size of this font.
     */
    public int size()
    {
        return size;
    }

    /**
     * Whether the weight of this font is bold or normal.
     */
    public boolean isBold()
    {
        return bold;
    }

    /**
     * Whether the style of this font is italic or normal.
     */
    public boolean isItalic()
    {
        return italic;
    }

    @Override
    public String toString()
    {
        return Font.class.getSimpleName()
                + ": name="   + name
                + ", size="   + size
                + ", bold="   + bold
                + ", italic=" + italic;
    }

}
