package com.calclipse.lib.resource;

import static java.util.stream.Collectors.toList;

import java.io.File;
import java.util.function.Predicate;
import java.util.stream.Stream;

import com.calclipse.lib.misc.StringUtil;

/**
 * Static methods for dealing with search paths.
 * 
 * @author Tone Sommerland
 */
public final class SearchPaths
{
    /**
     * Tests whether a path component is a jar file.
     * Checks if the filename ends with {@code ".jar"}.
     */
    public static final Predicate<File>
    IS_JAR = file -> file.getName().endsWith(".jar");
    
    /**
     * Tests whether a path component is a zip file.
     * Checks if the filename ends with {@code ".zip"}.
     */
    public static final Predicate<File>
    IS_ZIP = file -> file.getName().endsWith(".zip");
    
    private static final String PATH_SEPARATOR_KEY = "path.separator";
    private static final String MODULE_PATH_KEY = "jdk.module.path";
    private static final String CLASS_PATH_KEY = "java.class.path";
    
    private SearchPaths()
    {
    }
    
    /**
     * Makes a search path by
     * splitting a path string into individual path components
     * using a regex as the delimiter.
     * @param path the path string
     * @param separator a regex path-component delimiter
     */
    public static SearchPath split(final String path, final String separator)
    {
        return SearchPath.of(
                Stream.of(StringUtil.splitUnlessEmpty(path, separator))
                    .map(File::new)
                    .collect(toList()));
    }
    
    /**
     * Returns the module path.
     * If the module path is not present,
     * an empty string will be used in its stead.
     * @throws IllegalStateException
     * if the system property {@code "path.separator"} is missing
     */
    public static SearchPath modulePath()
    {
        final String path = System.getProperty(MODULE_PATH_KEY, "");
        return split(path, requireSystemProperty(PATH_SEPARATOR_KEY));
    }
    
    /**
     * Returns the class path.
     * If the class path is not present,
     * an empty string will be used in its stead.
     * @throws IllegalStateException
     * if the system property {@code "path.separator"} is missing
     */
    public static SearchPath classPath()
    {
        final String path = System.getProperty(CLASS_PATH_KEY, "");
        return split(path, requireSystemProperty(PATH_SEPARATOR_KEY));
    }
    
    /**
     * Returns the module path or the class path.
     * If neither the module path nor the class path are present,
     * an empty string will be used in stead.
     * @throws IllegalStateException
     * if the system property {@code "path.separator"} is missing
     */
    public static SearchPath moduleOrClassPath()
    {
        final String modulePath = System.getProperty(MODULE_PATH_KEY);
        final String classPath = System.getProperty(CLASS_PATH_KEY);
        final String path;
        if (modulePath != null)
        {
            path = modulePath;
        }
        else if (classPath != null)
        {
            path = classPath;
        }
        else
        {
            path = "";
        }
        return split(path, requireSystemProperty(PATH_SEPARATOR_KEY));
    }
    
    private static String requireSystemProperty(final String key)
    {
        final String value = System.getProperty(key);
        if (value == null)
        {
            throw new IllegalStateException(
                    "System property '" + key + "' is missing.");
        }
        return value;
    }

}
