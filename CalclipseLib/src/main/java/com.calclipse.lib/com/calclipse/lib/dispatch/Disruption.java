package com.calclipse.lib.dispatch;

/**
 * Thrown from {@link Dispatcher#consecutively(Dispatchable)}.
 * Since {@link Dispatchable#execute()} is declared to throw this exception,
 * it may be used by the implementing class to wrap any other exception
 * resulting from the execution.
 * 
 * @author Tone Sommerland
 */
public class Disruption extends Exception
{
    private static final long serialVersionUID = 1L;

    public Disruption(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    public Disruption(final String message)
    {
        super(message);
    }

    public Disruption(final Throwable cause)
    {
        super(cause);
    }

}
