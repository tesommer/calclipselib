package com.calclipse.lib.resource;

import static java.util.Objects.requireNonNull;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

/**
 * A resource on a search path.
 * A resource instance may correspond to
 * multiple files having the same resource name
 * found on different path components.
 * 
 * @author Tone Sommerland
 */
public interface Resource
{
    /**
     * Returns the next resource file, or empty of there are no one left.
     */
    public abstract Optional<InputStream> next() throws IOException;
    
    /**
     * Gives this resource to the given user.
     * This method calls
     * {@link ResourceUser#use(java.io.InputStream)}
     * for each resource file until the user returns {@code false}.
     * @implSpec This method calls {@link #next()}.
     */
    public default void use(final ResourceUser user) throws IOException
    {
        requireNonNull(user, "user");
        for (
                Optional<InputStream> nextToUse = next();
                nextToUse.isPresent();
                nextToUse = next())
        {
            try (final InputStream in = nextToUse.get())
            {
                if (!user.use(in))
                {
                    break;
                }
            }
        }
    }

}
