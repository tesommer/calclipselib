package com.calclipse.lib.dispatch;

import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

import com.calclipse.lib.dispatch.internal.DefaultDispatcher;

/**
 * Allows execution of code on a specific thread from any thread.
 * 
 * @author Tone Sommerland
 */
public interface Dispatcher
{
    /**
     * Returns an instance that delegates
     * {@link #isDestinationThread()}
     * and
     * {@link #concurrently(Runnable)}
     * to the given functions.
     */
    public static Dispatcher depute(
            final BooleanSupplier isDestThread,
            final Consumer<? super Runnable> concurrently)
    {
        return new DefaultDispatcher(isDestThread, concurrently);
    }
    
    /**
     * Checks if the current thread is the destination for this dispatcher.
     * This method can be called from any thread.
     */
    public abstract boolean isDestinationThread();
    
    /**
     * Executes a task concurrently on the destination thread.
     * This method returns immediately.
     * This method can be called from any thread.
     */
    public abstract void concurrently(Runnable doRun);
    
    /**
     * Executes a task consecutively on the destination thread.
     * This method blocks until execution finishes,
     * even when interrupted.
     * The interrupted status of the thread may be polled when finished.
     * This method may be called from any thread.
     * @param <T> type of return value
     * @param doRun the task to be executed
     * @return the result returned by the task
     * @throws Disruption if thrown by the task,
     * or if an exceptional condition occurs when attempting to
     * dispatch the task
     */
    public abstract <T> T consecutively(Dispatchable<T> doRun)
            throws Disruption;

}
