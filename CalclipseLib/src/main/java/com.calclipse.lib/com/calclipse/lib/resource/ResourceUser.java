package com.calclipse.lib.resource;

import java.io.IOException;
import java.io.InputStream;

/**
 * A resource user.
 * 
 * @author Tone Sommerland
 */
@FunctionalInterface
public interface ResourceUser
{
    /**
     * Processes a resource.
     * This method is called for each resource file
     * with the same resource name found on the search path
     * until {@code false} is returned.
     * The input stream is closed automatically.
     * @param in a resource file
     * @return whether or not to continue to the next one
     * @see com.calclipse.lib.resource.Resource#use(ResourceUser)
     */
    public abstract boolean use(InputStream in) throws IOException;

}
