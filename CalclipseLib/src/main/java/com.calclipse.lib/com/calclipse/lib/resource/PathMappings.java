package com.calclipse.lib.resource;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

/**
 * Static functions for altering search paths.
 * Altering a search path involves taking a set of path components
 * and turning it into another set of path components.
 * 
 * @see com.calclipse.lib.resource.SearchPath#map(Function)
 * 
 * @author Tone Sommerland
 */
public final class PathMappings
{
    /**
     * Accepts a search path and returns it as is.
     */
    public static final UnaryOperator<List<File>>
    AS_IS = UnaryOperator.identity();
    
    /**
     * Only path components that actually exist.
     */
    public static final UnaryOperator<List<File>>
    EXISTING = components -> components.stream()
        .filter(File::exists)
        .collect(toList());
        
    /**
     * Existing only and no duplicates.
     */
    public static final UnaryOperator<List<File>>
    EXISTING_AND_DISTINCT = components -> components.stream()
        .filter(File::exists)
        .map(PathMappings::canonical)
        .distinct()
        .collect(toList());

    private PathMappings()
    {
    }
    
    /**
     * Makes a search path mapping that includes
     * the result of two mappings of the same search path.
     */
    public static UnaryOperator<List<File>> concatenating(
            final Function<
                ? super List<File>,
                ? extends Collection<? extends File>> mapping1,
            final Function<
                ? super List<File>,
                ? extends Collection<? extends File>> mapping2)
    {
        requireNonNull(mapping1, "mapping1");
        requireNonNull(mapping2, "mapping2");
        return components -> concat(
                mapping1.apply(components), mapping2.apply(components));
    }
    
    /**
     * Children of directories on the search path.
     * @param include which children to include
     */
    public static UnaryOperator<List<File>> directoryChildren(
            final Predicate<? super File> include)
    {
        requireNonNull(include, "include");
        return components -> components.stream()
                .filter(File::isDirectory)
                .map(File::listFiles)
                .flatMap(Stream::of)
                .filter(include)
                .collect(toList());
    }
    
    private static File canonical(final File file)
    {
        try
        {
            return file.getCanonicalFile();
        }
        catch (final IOException ex)
        {
            return file;
        }
    }
    
    private static List<File> concat(
            final Collection<? extends File> first,
            final Collection<? extends File> second)
    {
        return Stream.concat(first.stream(), second.stream()).collect(toList());
    }

}
