package com.calclipse.lib.paint;

/**
 * An image made up of a rectangular array of pixels.
 * 
 * @author Tone Sommerland
 */
public interface Pixmap
{
    /**
     * The width of this image.
     */
    public abstract int width();

    /**
     * The height of this image.
     */
    public abstract int height();

    /**
     * Returns a context for drawing upon this image.
     */
    public abstract PaintContext paint();
    
    /**
     * Returns the pixel color at the specified coordinates.
     * @throws IllegalArgumentException
     * if {@code x} is less than zero
     * or greater than or equal to {@link #width()},
     * or if {@code y} is less than zero
     * or greater than or equal to {@link #height()}
     */
    public abstract Rgb sample(int x, int y);
}
