package com.calclipse.lib.misc;

/**
 * String-related utility methods.
 * 
 * @author Tone Sommerland
 */
public final class StringUtil
{
    private StringUtil()
    {
    }
    
    /**
     * An alternative to {@code String.split}
     * that returns an empty array if the string to split is empty.
     * @throws java.util.regex.PatternSyntaxException if the regex is invalid
     */
    public static String[] splitUnlessEmpty(
            final String str, final String regex, final int limit)
    {
        if (str.isEmpty())
        {
            return new String[0];
        }
        return str.split(regex, limit);
    }
    
    /**
     * Calls {@link #splitUnlessEmpty(String, String, int)}
     * with a limit of zero.
     * @throws java.util.regex.PatternSyntaxException if the regex is invalid
     */
    public static String[] splitUnlessEmpty(
            final String str, final String regex)
    {
        return splitUnlessEmpty(str, regex, 0);
    }
    
    /**
     * Similar to {@code String.endsWith},
     * but accepts char sequences.
     */
    public static boolean endsWith(
            final CharSequence text, final CharSequence suffix)
    {
        if (suffix.length() == 0)
        {
            return true;
        }
        if (text.length() < suffix.length())
        {
            return false;
        }
        for (int i = 0; i < suffix.length(); i++)
        {
            final char c1 = text.charAt(text.length() - suffix.length() + i);
            final char c2 = suffix.charAt(i);
            if (c1 != c2)
            {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Returns {@code true}
     * if the given text is a few characters short of ending
     * with the specified suffix.
     * E.g. {@code endsWithPartially("te htt", "http") == true},
     * but {@code endsWithPartially("te http", "http") == false}
     */
    public static boolean endsWithPartially(
            final CharSequence text, final CharSequence suffix)
    {
        for (int i = 1; i < suffix.length(); i++)
        {
            if (endsWith(text, suffix.subSequence(0, i)))
            {
                return true;
            }
        }
        return false;
    }

}
