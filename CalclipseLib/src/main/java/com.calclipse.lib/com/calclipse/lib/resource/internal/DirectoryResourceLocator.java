package com.calclipse.lib.resource.internal;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.calclipse.lib.resource.ResourceLocator;

/**
 * A resource locator that searches directories on the search path.
 * 
 * @author Tone Sommerland
 */
public enum DirectoryResourceLocator implements ResourceLocator
{
    INSTANCE;
    
    private static final String NAME_SEPARATOR = "/";
    
    @Override
    public InputStream find(final String name, final File pathComponent)
            throws IOException
    {
        if (!pathComponent.isDirectory())
        {
            return null;
        }
        if (name.startsWith(NAME_SEPARATOR))
        {
            return find(name.substring(1), pathComponent);
        }
        return searchDir(pathComponent, name.split(NAME_SEPARATOR));
    }

    private static InputStream searchDir(
            final File dir, final String[] nameParts) throws IOException
    {
        File child = dir;
        for (final String namePart : nameParts)
        {
            child = new File(child, namePart);
            if (!child.exists())
            {
                return null;
            }
        }
        return new FileInputStream(child);
    }

}
