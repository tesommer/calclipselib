package com.calclipse.lib.arrange;

import java.util.Collection;

/**
 * An arrangement of objects.
 * An implementation has specific rules on where its elements are inserted
 * based on the elements' {@link Arrangable#key() keys}.
 * The keys of the elements in an arrangement are allowed to change.
 * However, when calling any of the methods
 * {@link #add(Arrangable)},
 * {@link #insertionIndexOf(int)},
 * {@link #interval(int, int)}
 * or
 * {@link #removeInterval(int, int)},
 * the keys must be in compliance with the rules of the arrangement.
 * The first position in the arrangement has index zero.
 * 
 * @see Arrangable
 * 
 * @author Tone Sommerland
 * @param <T> The type of object in this arrangement.
 */
public interface Arrangement<T extends Arrangable> extends Iterable<T>
{
    /**
     * Adds an element to this arrangement.
     * @return the index of the element after it was added
     */
    public abstract int add(T element);
    
    /**
     * Removes the element at the specified index.
     * @return the element that was removed
     * @throws IndexOutOfBoundsException if the index is invalid
     */
    public abstract T remove(int index);
    
    /**
     * Removes the given element from this arrangement if present.
     * @return {@code true} if the element was removed,
     * {@code false} if it wasn't found
     */
    public abstract boolean remove(Object element);
    
    /**
     * Returns the element at the specified index.
     * @throws IndexOutOfBoundsException if the index is invalid
     */
    public abstract T get(int index);
    
    /**
     * Removes all elements in this arrangement.
     */
    public abstract void clear();
    
    /**
     * The number of elements in this arrangement.
     */
    public abstract int size();
    
    /**
     * Returns the index that an element with the given key would get
     * if it was added.
     */
    public abstract int insertionIndexOf(int key);
    
    /**
     * Returns the elements in this arrangement with keys
     * greater than or equal to {@code fromKey},
     * and less than {@code toKey}.
     */
    public abstract Collection<T> interval(int fromKey, int toKey);
    
    /**
     * Removes elements in this arrangement with keys
     * greater than or equal to {@code fromKey},
     * and less than {@code toKey}.
     * @return the removed elements
     */
    public abstract Collection<T> removeInterval(int fromKey, int toKey);
    
    /**
     * Whether or not this arrangement is empty.
     * @implSpec This implementation returns {@code true}
     * if {@link #size()} returns zero.
     */
    public default boolean isEmpty()
    {
        return size() == 0;
    }

}
