package com.calclipse.lib.arrange;

/**
 * Implementing this interface allows an object to be put in an
 * {@link Arrangement}.
 * As an example, an object implementing this interface
 * could represent a position in a text document being edited.
 * 
 * @author Tone Sommerland
 */
public interface Arrangable
{
    /**
     * Depending on the arrangement,
     * this key determines where this element will be
     * placed relative to the other elements in the arrangement.
     */
    public abstract int key();

}
