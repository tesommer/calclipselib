package com.calclipse.lib.paint;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * Provides methods for custom drawing.
 * 
 * @author Tone Sommerland
 */
public interface PaintContext extends AutoCloseable
{
    /**
     * Makes an off-screen image with the specified dimensions.
     * @param width the width in pixels
     * @param height the height in pixels
     * @throws IllegalArgumentException if width or height is less than zero
     */
    public abstract Pixmap makeImage(int width, int height);
    
    /**
     * Reads an image from the given input stream.
     */
    public abstract Pixmap readImage(InputStream in) throws IOException;
    
    /**
     * Sets the font of this context.
     * @return {@code false} if the given font was not supported
     */
    public abstract boolean setFont(Font font);
    
    /**
     * Sets this context's font specified with the given properties.
     * @param properties the font properties
     * @return {@code false} if the given font was not supported
     * @implSpec Calls {@link Font#fromProperties(Map)}.
     */
    public default boolean setFont(final Map<String, String> properties)
    {
        return Font.fromProperties(properties).map(this::setFont).orElse(false);
    }
    
    /**
     * Returns the metrics of this context's current font.
     */
    public abstract FontMetrics fontMetrics();
    
    /**
     * Sets the color of this context.
     * return {@code this}
     */
    public abstract PaintContext setColor(Rgb color);
    
    /**
     * Sets the clip of this context.
     * @param x the left of the clip bounds
     * @param y the top of the clip bounds
     * @param width the width of the clip bounds
     * @param height the height of the clip bounds
     * return {@code this}
     */
    public abstract PaintContext setClip(int x, int y, int width, int height);
    
    /**
     * Translates the origin of this context to the given coordinates.
     * return {@code this}
     */
    public abstract PaintContext translate(int x, int y);
    
    /**
     * Draws a line between two points specified by the given coordinates.
     * @param x1 start x
     * @param y1 start y
     * @param x2 end x
     * @param y2 end y
     * return {@code this}
     */
    public abstract PaintContext line(int x1, int y1, int x2, int y2);
    
    /**
     * Draws a rectangle.
     * @param fill whether or not to fill the rectangle
     * @param x left
     * @param y top
     * @param width width
     * @param height height
     * return {@code this}
     */
    public abstract PaintContext rectangle(
            boolean fill, int x, int y, int width, int height);
    
    /**
     * Draws a circle or ellipse that fits within the specified bounds.
     * @param fill whether or not to fill the oval
     * @param x left
     * @param y top
     * @param width width
     * @param height height
     * return {@code this}
     */
    public abstract PaintContext oval(
            boolean fill, int x, int y, int width, int height);
    
    /**
     * Draws a circular or elliptical arc that fits within the specified bounds.
     * @param fill whether or not to fill the arc
     * @param x left
     * @param y top
     * @param width width
     * @param height height
     * @param startAngle start angle
     * @param endAngle end angle
     * return {@code this}
     */
    public abstract PaintContext arc(
            boolean fill,
            int x,
            int y,
            int width,
            int height,
            int startAngle,
            int endAngle);
    
    /**
     * Draws a polygon specified by the given vertex coordinates.
     * @param fill whether or not to fill the polygon
     * @param xy an array containing x1, y1, x2, y2 etc.
     * return {@code this}
     */
    public abstract PaintContext polygon(boolean fill, int... xy);
    
    /**
     * Draws the given string at the specified point.
     * @param str the text to draw
     * @param x left
     * @param y top
     * return {@code this}
     */
    public abstract PaintContext text(String str, int x, int y);
    
    /**
     * Draws a scaled image.
     * @param pixmap the image
     * @param x left
     * @param y top
     * @param width width
     * @param height height
     * return {@code this}
     */
    public abstract PaintContext image(
            Pixmap pixmap, int x, int y, int width, int height);
    
    /**
     * Draws an image.
     * @param pixmap the image
     * @param x left
     * @param y top
     * return {@code this}
     * @implSPec Calls {@link #image(Pixmap, int, int, int, int)}
     * with the image's width and height.
     */
    public default PaintContext image(
            final Pixmap pixmap, final int x, final int y)
    {
        return image(pixmap, x, y, pixmap.width(), pixmap.height());
    }
    
    /**
     * Erases a region specified by the given bounds.
     * @param x left
     * @param y top
     * @param width width
     * @param height height
     * return {@code this}
     */
    public abstract PaintContext clear(int x, int y, int width, int height);
    
    /**
     * Disposes of this context and releases any resources held by it.
     */
    public abstract void close();
}
