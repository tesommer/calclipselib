package com.calclipse.lib.dispatch.internal;

import static java.util.Objects.requireNonNull;

import com.calclipse.lib.dispatch.Disruption;

/**
 * Execution result wrapper used by the default dispatcher.
 * An instance contains either a value or a disruption.
 * 
 * @author Tone Sommerland
 *
 * @param <T> type resulting from the execution.
 */
public final class Result<T>
{
    private final T value;
    private final Disruption disruption;
    
    private Result(final T value, final Disruption disruption)
    {
        this.value = value;
        this.disruption = disruption;
    }
    
    /**
     * Creates an instance containing a value.
     * @param value the value (nullable)
     */
    public static <T> Result<T> ofValue(final T value)
    {
        return new Result<>(value, null);
    }
    
    /**
     * Creates an instance containing a disruption.
     * @param disruption the disruption
     */
    public static <T> Result<T> ofDisruption(final Disruption disruption)
    {
        return new Result<>(null, requireNonNull(disruption, "disruption"));
    }

    /**
     * Returns the value wrapped by this instance.
     * @throws Disruption if wrapping an disruption
     */
    public T get() throws Disruption
    {
        if (disruption != null)
        {
            throw disruption;
        }
        return value;
    }

    @Override
    public String toString()
    {
        return Result.class.getSimpleName()
                + disruption == null ? ": value" : ": disruption";
    }

}
