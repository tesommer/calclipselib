package com.calclipse.lib.dispatch;

/**
 * A task that can be given to a {@link Dispatcher}
 * for synchronous execution
 * on a destination thread determined by the dispatcher.
 * 
 * @author Tone Sommerland
 *
 * @param <T> Type of object returned from {@link #execute()}.
 */
@FunctionalInterface
public interface Dispatchable<T>
{
    /**
     * This method is called from the destination thread of the dispatcher.
     * The implementation of this method may throw
     * {@link Disruption} if necessary.
     * @return the result of the execution
     * @throws Disruption if thrown by the implementation
     */
    public abstract T execute() throws Disruption;

}
