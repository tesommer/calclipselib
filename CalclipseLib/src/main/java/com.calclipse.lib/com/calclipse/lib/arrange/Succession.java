package com.calclipse.lib.arrange;

import static java.util.stream.Collectors.joining;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.function.IntFunction;

/**
 * An ordered sequence of elements.
 * Elements are ordered such that when adding a new element,
 * its index will be greater than any other element with a key less than
 * or equal to the key of the added element, but less than any element
 * with a key greater than the added element's key.
 * Thus, the arrangement rule is the standard order for natural numbers
 * with new elements being inserted at the highest possible index.
 * The iterator returned by this class supports removal.
 * 
 * @author Tone Sommerland
 */
public final class Succession<T extends Arrangable> implements Arrangement<T>
{
    private final List<T> elements = new ArrayList<>();

    public Succession()
    {
    }

    @Override
    public Iterator<T> iterator()
    {
        return elements.iterator();
    }

    @Override
    public Spliterator<T> spliterator()
    {
        return elements.spliterator();
    }

    @Override
    public int add(final T element)
    {
        final int index = calculateInsertionIndex(element.key());
        elements.add(index, element);
        return index;
    }

    @Override
    public T remove(final int index)
    {
        return elements.remove(index);
    }

    @Override
    public boolean remove(final Object element)
    {
        return elements.remove(element);
    }

    @Override
    public T get(final int index)
    {
        return elements.get(index);
    }

    @Override
    public void clear()
    {
        elements.clear();
    }

    @Override
    public int size()
    {
        return elements.size();
    }

    @Override
    public int insertionIndexOf(final int key)
    {
        return calculateInsertionIndex(key);
    }

    @Override
    public Collection<T> interval(final int fromKey, final int toKey)
    {
        return withFromTo(fromKey, toKey, elements::get);
    }

    @Override
    public Collection<T> removeInterval(final int fromKey, final int toKey)
    {
        return withFromTo(fromKey, toKey, elements::remove);
    }
    
    @Override
    public String toString()
    {
        return Succession.class.getSimpleName() + elements.stream()
            .map(Arrangable::key)
            .map(String::valueOf)
            .collect(joining(", ", ": ", ""));
    }

    private Collection<T> withFromTo(
            final int fromKey, final int toKey, final IntFunction<T> doThis)
    {
        final var result = new ArrayList<T>();
        for (int index = calculateInsertionIndex(toKey); index > 0;)
        {
            final int key = elements.get(--index).key();
            if (key >= fromKey && key < toKey)
            {
                result.add(doThis.apply(index));
            }
            else if (key < fromKey)
            {
                break;
            }
        }
        return result;
    }

    private int calculateInsertionIndex(final int key)
    {
        if (elements.isEmpty())
        {
            return 0;
        }
        int min = 0;
        int max = elements.size();
        int index;
        int upOrDown;
        do
        {
            index = min + (max - min) / 2;
            upOrDown = compareToKeyAtIndex(key, index);
            if (upOrDown > 0)
            {
                min = index + 1;
            }
            else
            {
                max = index - 1;
            }
        }
        while (upOrDown != 0 && min <= max);
        return index;
    }

    private int compareToKeyAtIndex(final int key, final int index)
    {
        if (index == 0)
        {
            return compareToKeyAtStartIndex(key);
        }
        else if (index == elements.size())
        {
            return compareToKeyAtEndIndex(key);
        }
        else
        {
            return compareToKeyAtMidIndex(key, index);
        }
    }

    private int compareToKeyAtMidIndex(final int key, final int index)
    {
        if (key < elements.get(index).key()
                && key >= elements.get(index - 1).key())
        {
            return 0;
        }
        else if (key >= elements.get(index).key())
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }

    private int compareToKeyAtEndIndex(final int key)
    {
        if (key >= elements.get(elements.size() - 1).key())
        {
            return 0;
        }
        return -1;
    }

    private int compareToKeyAtStartIndex(final int key)
    {
        if (key < elements.get(0).key())
        {
            return 0;
        }
        return 1;
    }

}
