package com.calclipse.lib.paint;

/**
 * A color specified with a red, green, blue and an alpha component,
 * each with a value from 0 to 255, inclusive.
 * 
 * @author Tone Sommerland
 */
public final class Rgb
{
    private static final int OPAQUE = 255;
    
    private final int red;
    private final int green;
    private final int blue;
    private final int alpha;
    
    private Rgb(final int red, final int green, final int blue, final int alpha)
    {
        this.red   = requireByte(red,   "red"  );
        this.green = requireByte(green, "green");
        this.blue  = requireByte(blue,  "blue" );
        this.alpha = requireByte(alpha, "alpha");
    }
    
    /**
     * Returns a color with an alpha value of 255.
     * @param red the red component
     * @param green the green component
     * @param blue the blue component
     * @throws IllegalArgumentException
     * if any of the components is less than zero or greater than 255
     */
    public static Rgb opaque(final int red, final int green, final int blue)
    {
        return new Rgb(red, green, blue, OPAQUE);
    }
    
    /**
     * Returns a color with a degree of transparency.
     * An alpha value of 0 is entirely transparent, and 255 is opaque.
     * @param red the red component
     * @param green the green component
     * @param blue the blue component
     * @param alpha the alpha component
     * @throws IllegalArgumentException
     * if any of the components is less than zero or greater than 255
     */
    public static Rgb withTransparency(
            final int red, final int green, final int blue, final int alpha)
    {
        return new Rgb(red, green, blue, alpha);
    }
    
    /**
     * Returns a color similar to this, but with the given red component.
     */
    public Rgb withRed(final int newRed)
    {
        return new Rgb(newRed, this.green, this.blue, this.alpha);
    }

    /**
     * Returns a color similar to this, but with the given green component.
     */
    public Rgb withGreen(final int newGreen)
    {
        return new Rgb(this.red, newGreen, this.blue, this.alpha);
    }

    /**
     * Returns a color similar to this, but with the given blue component.
     */
    public Rgb withBlue(final int newBlue)
    {
        return new Rgb(this.red, this.green, newBlue, this.alpha);
    }

    /**
     * Returns a color similar to this, but with the given alpha component.
     */
    public Rgb withAlpha(final int newAlpha)
    {
        return new Rgb(this.red, this.green, this.blue, newAlpha);
    }

    /**
     * The red component of this color.
     */
    public int red()
    {
        return red;
    }

    /**
     * The green component of this color.
     */
    public int green()
    {
        return green;
    }

    /**
     * The blue component of this color.
     */
    public int blue()
    {
        return blue;
    }

    /**
     * The alpha component of this color.
     */
    public int alpha()
    {
        return alpha;
    }
    
    @Override
    public String toString() {
        return Rgb.class.getSimpleName()
                + ": red="   + red
                + ", green=" + green
                + ", blue="  + blue
                + ", alpha=" + alpha;
    }

    private static int requireByte(
            final int component,
            final String componentName) throws IllegalArgumentException
    {
        if (component < 0 || component > 255)
        {
            throw new IllegalArgumentException(
                    componentName + " not in [0, 255]: " + component);
        }
        return component;
    }

}
