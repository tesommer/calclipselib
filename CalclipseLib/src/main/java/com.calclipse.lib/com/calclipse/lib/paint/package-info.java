/**
 * This package contains an abstraction API for custom drawing.
 */
package com.calclipse.lib.paint;
