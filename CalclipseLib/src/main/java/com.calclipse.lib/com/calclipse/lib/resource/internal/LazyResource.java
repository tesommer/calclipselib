package com.calclipse.lib.resource.internal;

import static java.util.Objects.requireNonNull;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import com.calclipse.lib.resource.Resource;
import com.calclipse.lib.resource.ResourceLocator;
import com.calclipse.lib.resource.SearchPath;

/**
 * A find-by-need resource.
 * 
 * @author Tone Sommerland
 */
public final class LazyResource implements Resource
{
    private final String name;
    private final SearchPath path;
    private final ResourceLocator locator;
    private int pathIndex;

    public LazyResource(
            final String name,
            final SearchPath path,
            final ResourceLocator locator)
    {
        this.name = requireNonNull(name, "name");
        this.path = requireNonNull(path, "path");
        this.locator = requireNonNull(locator, "locator");
    }

    @Override
    public Optional<InputStream> next() throws IOException
    {
        while (pathIndex < path.size())
        {
            final File pathComponent = path.get(pathIndex++);
            final InputStream in = locator.find(name, pathComponent);
            if (in != null)
            {
                return Optional.of(in);
            }
        }
        return Optional.empty();
    }

    @Override
    public String toString()
    {
        return name;
    }

}
