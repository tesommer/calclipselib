/**
 * This package provides a mechanism for locating resources on search paths.
 */
package com.calclipse.lib.resource;
