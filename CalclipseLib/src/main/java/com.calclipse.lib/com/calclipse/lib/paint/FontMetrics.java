package com.calclipse.lib.paint;

import java.util.Map;

/**
 * Information about the rendering of a font.
 * 
 * @author Tone Sommerland
 */
public interface FontMetrics
{
    public static final String KEY_ASCENT  = "ascent";
    public static final String KEY_DESCENT = "descent";
    public static final String KEY_ADVANCE = "advance";
    public static final String KEY_LEADING = "leading";
    public static final String KEY_HEIGHT  = "height";
    
    /**
     * Returns a map containing font rendering measurements.
     */
    public abstract Map<String, Double> measures();
    
    /**
     * Returns a map containing measurements regarding
     * rendering of the given text.
     */
    public abstract Map<String, Double> measure(CharSequence text);
}
