/**
 * This package contains a data structure that
 * arranges elements in a specific order based on numeric keys.
 */
package com.calclipse.lib.arrange;
