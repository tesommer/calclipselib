package com.calclipse.lib.resource;

import static com.calclipse.lib.resource.PathMappings.AS_IS;
import static com.calclipse.lib.resource.PathMappings.EXISTING_AND_DISTINCT;
import static com.calclipse.lib.resource.PathMappings.concatenating;
import static com.calclipse.lib.resource.PathMappings.directoryChildren;
import static com.calclipse.lib.resource.ResourceLocator.archives;
import static com.calclipse.lib.resource.ResourceLocator.directories;
import static com.calclipse.lib.resource.SearchPaths.IS_JAR;
import static com.calclipse.lib.resource.SearchPaths.IS_ZIP;
import static com.calclipse.lib.resource.SearchPaths.moduleOrClassPath;

import java.io.File;
import java.util.function.Predicate;

import com.calclipse.lib.resource.internal.LazyResource;

/**
 * Static utility methods for locating resources on search paths.
 * A resource represents one or more files with the same resource name.
 * The name format is determined by
 * {@link com.calclipse.lib.resource.ResourceLocator}s.
 * However, the standard locators use the same format as
 * {@code java.lang.Class.getResource}
 * with the name starting with
 * {@code '/'}.
 * 
 * @author Tone Sommerland
 */
public final class Resources
{
    private Resources()
    {
    }
    
    /**
     * Finds the resource with the given name.
     * @param name the resource name
     * @param path the search path
     * @param locator the locator to use
     */
    public static Resource find(
            final String name,
            final SearchPath path,
            final ResourceLocator locator)
    {
        return new LazyResource(name, path, locator);
    }
    
    /**
     * Finds the resource with the given name
     * by searching the module path or class path.
     * Directories, jars and zips are searched.
     * If any directories on the search path contain zips or jars,
     * those zips and jars are searched as well.
     * @param name the resource name
     */
    public static Resource findOnModuleOrClassPath(final String name)
    {
        final Predicate<File> jarsAndZips = IS_JAR.or(IS_ZIP);
        final SearchPath path = moduleOrClassPath().map(
                EXISTING_AND_DISTINCT.andThen(concatenating(
                        AS_IS, directoryChildren(jarsAndZips))));
        final ResourceLocator locator
            = directories().combinedWith(archives(jarsAndZips));
        return find(name, path, locator);
    }

}
