package com.calclipse.lib.misc;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Reference-related utility methods.
 * 
 * @author Tone Sommerland
 */
public final class RefUtil
{
    private RefUtil()
    {
    }
    
    /**
     * Removes references found on the queue from the given collection.
     */
    public static void expungeDeadReferences(
            final Collection<? extends Reference<?>> references,
            final ReferenceQueue<?> refQueue)
    {
        for (
                Reference<?> ref = refQueue.poll();
                ref != null;
                ref = refQueue.poll())
        {
            references.remove(ref);
        }
    }
    
    /**
     * Makes a list of the objects that are still
     * referenced from the given collection.
     * @param expungeDeadRefs true to remove dead references
     */
    public static <T> List<T> getLiveReferents(
            final Collection<? extends Reference<T>> references,
            final boolean expungeDeadRefs)
    {
        final var liveOnes = new ArrayList<T>(references.size());
        for (final var it = references.iterator(); it.hasNext();)
        {
            final T referent = it.next().get();
            if (referent == null)
            {
                if (expungeDeadRefs)
                {
                    it.remove();
                }
            }
            else
            {
                liveOnes.add(referent);
            }
        }
        return liveOnes;
    }

}
