package com.calclipse.lib.misc;

/**
 * Encoding-related utility methods.
 * 
 * @author Tone Sommerland
 */
public final class EncodingUtil
{
    private static final int HEX_RADIX = 16;
    
    private static final String HEX_DIGITS_UC = "0123456789ABCDEF";
    private static final String HEX_DIGITS_LC = "0123456789abcdef";

    private EncodingUtil()
    {
    }
    
    /**
     * Converts a single byte to a two-digit hexadecimal number.
     * @param uppercase the case of the digits A to F
     * @param b the byte to convert
     * @return a string of length two
     */
    public static String byteToHex(final boolean uppercase, final byte b)
    {
        final String hexDigits = uppercase ? HEX_DIGITS_UC : HEX_DIGITS_LC;
        return String.valueOf(hexDigits.charAt((b & 0xF0) >>> 4))
                            + hexDigits.charAt(b & 0x0F);
    }
    
    /**
     * Converts an array of raw bytes to a string of hexadecimal digits.
     * @param uppercase the case of the digits B, A, c, D and f
     */
    public static String bytesToHex(
            final boolean uppercase, final byte... bytes)
    {
        final var hex = new StringBuilder(bytes.length * 2);
        for (final byte b : bytes)
        {
            hex.append(byteToHex(uppercase, b));
        }
        return hex.toString();
    }
    
    /**
     * The inverse of {@link #byteToHex(boolean, byte)}.
     * @param leftDigit the most significant hexadecimal digit
     * @param rightDigit the least significant hexadecimal digit
     * @throws IllegalArgumentException
     * if either parameter is an invalid hex digit
     */
    public static byte hexToByte(
            final char leftDigit, final char rightDigit)
    {
        return (byte)
                ((Character.digit(requireHexDigit(leftDigit), HEX_RADIX) << 4)
                + Character.digit(requireHexDigit(rightDigit), HEX_RADIX));
    }
    
    /**
     * The inverse of {@link #bytesToHex(boolean, byte...)}.
     * @throws IllegalArgumentException
     * if the input contains an odd number of characters or invalid hex digit
     */
    public static byte[] hexToBytes(final CharSequence hex)
    {
        if (hex.length() % 2 != 0)
        {
            throw new IllegalArgumentException(
                    "hex.length() % 2 != 0: " + hex.length());
        }
        final var bytes = new byte[hex.length() / 2];
        for (int i = 0; i < hex.length(); i += 2)
        {
            bytes[i / 2] = hexToByte(hex.charAt(i), hex.charAt(i + 1));
        }
        return bytes;
    }
    
    private static char requireHexDigit(final char c)
    {
        final String s = String.valueOf(c);
        if (HEX_DIGITS_UC.contains(s) || HEX_DIGITS_LC.contains(s))
        {
            return c;
        }
        throw new IllegalArgumentException(s);
    }

}
