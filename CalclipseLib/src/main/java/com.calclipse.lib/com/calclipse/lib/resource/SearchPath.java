package com.calclipse.lib.resource;

import static java.util.stream.Collectors.joining;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

/**
 * An immutable object that encapsulates the components of a search path.
 * 
 * @author Tone Sommerland
 */
public final class SearchPath
{
    private final List<File> components;

    private SearchPath(final Collection<? extends File> components)
    {
        this.components = List.copyOf(components);
    }
    
    /**
     * Returns a search path made up of the given path components.
     */
    public static SearchPath of(final Collection<? extends File> components)
    {
        return new SearchPath(components);
    }
    
    /**
     * Maps this search path to another.
     * @param mapping the mapping to apply
     * @return the result of the mapping 
     */
    public SearchPath map(
            final Function<
                ? super List<File>,
                ? extends Collection<? extends File>> mapping)
    {
        return new SearchPath(mapping.apply(components));
    }
    
    /**
     * Returns the path component at the given index.
     * @param index a zero-based index
     * @throws IndexOutOfBoundsException if the given index is invalid
     */
    public File get(final int index)
    {
        return components.get(index);
    }
    
    /**
     * The number of components of this search path.
     */
    public int size()
    {
        return components.size();
    }

    /**
     * The components of this search path.
     */
    public List<File> components()
    {
        return components;
    }

    @Override
    public String toString()
    {
        return SearchPath.class.getSimpleName() + components.stream()
            .map(File::getName)
            .collect(joining(", ", ": ", ""));
    }

}
