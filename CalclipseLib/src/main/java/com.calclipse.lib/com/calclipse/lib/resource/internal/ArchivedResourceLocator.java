package com.calclipse.lib.resource.internal;

import static java.util.Objects.requireNonNull;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.function.Predicate;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.calclipse.lib.resource.ResourceLocator;

/**
 * A resource locator that searches archives on the search path.
 * When a resource is found, it is loaded into memory.
 * 
 * @author Tone Sommerland
 */
public final class ArchivedResourceLocator implements ResourceLocator
{
    private final Predicate<? super File> searchable;

    /**
     * Creates a new locator for archived resources.
     * @param searchable which path components to search
     */
    public ArchivedResourceLocator(final Predicate<? super File> searchable)
    {
        this.searchable = requireNonNull(searchable, "searchable");
    }

    @Override
    public InputStream find(final String name, final File pathComponent)
            throws IOException
    {
        if (!searchable.test(pathComponent))
        {
            return null;
        }
        try (
            final var zipFile = new ZipFile(pathComponent, ZipFile.OPEN_READ);
        )
        {
            final String unslashed = name.startsWith("/")
                    ? name.substring(1) : name;
            final ZipEntry entry = zipFile.getEntry(unslashed);
            if (entry == null)
            {
                return null;
            }
            return new ByteArrayInputStream(
                    zipFile.getInputStream(entry).readAllBytes());
        }
    }

}
