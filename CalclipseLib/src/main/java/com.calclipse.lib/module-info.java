/**
 * A general-purpose library.
 */
module com.calclipse.lib
{
    exports com.calclipse.lib.arrange;
    exports com.calclipse.lib.dispatch;
    exports com.calclipse.lib.misc;
    exports com.calclipse.lib.paint;
    exports com.calclipse.lib.resource;
}
