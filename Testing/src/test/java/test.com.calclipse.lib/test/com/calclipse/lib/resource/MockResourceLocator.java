package test.com.calclipse.lib.resource;

import static java.util.Objects.requireNonNull;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import com.calclipse.lib.resource.ResourceLocator;

/**
 * A mock resource locator.
 * 
 * @author Tone Sommerland
 */
public final class MockResourceLocator implements ResourceLocator
{
    private final File file;
    private final InputStream in;

    /**
     * Creates a resource locator
     * that finds the given input stream on the given file.
     */
    public MockResourceLocator(final File file, final InputStream in)
    {
        this.file = requireNonNull(file, "file");
        this.in = requireNonNull(in, "in");
    }
    
    /**
     * Creates a resource locator that never finds anything.
     */
    public MockResourceLocator()
    {
        this.file = null;
        this.in = null;
    }

    @Override
    public InputStream find(final String name, final File pathComponent)
            throws IOException
    {
        if (file == null)
        {
            return null;
        }
        if (file.equals(pathComponent)) 
        {
            return in;
        }
        return null;
    }

}
