package test.com.calclipse.lib.paint;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Map;

import org.junit.jupiter.api.Test;

import com.calclipse.lib.paint.Font;

public final class FontTest
{
    private static final String TEN = "10";
    private static final String HELVETICA = "Helvetica";

    public FontTest()
    {
    }
    
    @Test
    public void
    font_from_properties_without_name_is_empty
    ()
    {
        assertTrue(Font.fromProperties(Map.of(
                Font.KEY_SIZE,   TEN,
                Font.KEY_WEIGHT, Font.VALUE_WEIGHT_BOLD,
                Font.KEY_STYLE,  Font.VALUE_STYLE_ITALIC
                )).isEmpty());
    }
    
    @Test
    public void
    font_from_properties_without_size_is_empty
    ()
    {
        assertTrue(Font.fromProperties(Map.of(
                Font.KEY_NAME,   HELVETICA,
                Font.KEY_WEIGHT, Font.VALUE_WEIGHT_BOLD,
                Font.KEY_STYLE,  Font.VALUE_STYLE_ITALIC
                )).isEmpty());
    }
    
    @Test
    public void
    font_from_properties_with_non_integer_size_is_empty
    ()
    {
        assertTrue(Font.fromProperties(Map.of(
                Font.KEY_NAME,   HELVETICA,
                Font.KEY_SIZE,   "10.0",
                Font.KEY_WEIGHT, Font.VALUE_WEIGHT_BOLD,
                Font.KEY_STYLE,  Font.VALUE_STYLE_ITALIC
                )).isEmpty());
    }
    
    @Test
    public void
    font_from_properties_with_weight_bold_is_bold
    ()
    {
        assertTrue(Font.fromProperties(Map.of(
                Font.KEY_NAME,   HELVETICA,
                Font.KEY_SIZE,   TEN,
                Font.KEY_WEIGHT, Font.VALUE_WEIGHT_BOLD
                )).get().isBold());
    }
    
    @Test
    public void
    font_from_properties_with_weight_not_bold_is_not_bold
    ()
    {
        assertFalse(Font.fromProperties(Map.of(
                Font.KEY_NAME,   HELVETICA,
                Font.KEY_SIZE,   TEN
                )).get().isBold());
        assertFalse(Font.fromProperties(Map.of(
                Font.KEY_NAME,   HELVETICA,
                Font.KEY_SIZE,   TEN,
                Font.KEY_WEIGHT, "Super hard"
                )).get().isBold());
    }
    
    @Test
    public void
    font_from_properties_with_style_italic_is_italic
    ()
    {
        assertTrue(Font.fromProperties(Map.of(
                Font.KEY_NAME,   HELVETICA,
                Font.KEY_SIZE,   TEN,
                Font.KEY_STYLE,  Font.VALUE_STYLE_ITALIC
                )).get().isItalic());
    }
    
    @Test
    public void
    font_from_properties_with_style_not_italic_is_not_italic
    ()
    {
        assertFalse(Font.fromProperties(Map.of(
                Font.KEY_NAME,   HELVETICA,
                Font.KEY_SIZE,   TEN
                )).get().isItalic());
        assertFalse(Font.fromProperties(Map.of(
                Font.KEY_NAME,   HELVETICA,
                Font.KEY_SIZE,   TEN,
                Font.KEY_STYLE,  "VSCO"
                )).get().isItalic());
    }
    
    @Test
    public void
    supported_font_properties_are_name_size_weight_and_style
    ()
    {
        assertFont(
                HELVETICA,
                10,
                true,
                true,
                Font.fromProperties(Map.of(
                        Font.KEY_NAME,   HELVETICA,
                        Font.KEY_SIZE,   TEN,
                        Font.KEY_WEIGHT, Font.VALUE_WEIGHT_BOLD,
                        Font.KEY_STYLE,  Font.VALUE_STYLE_ITALIC
                        )).get());
    }
    
    @Test
    public void
    unsupported_font_properties_are_ignored
    ()
    {
        assertFont(
                HELVETICA,
                10,
                false,
                false,
                Font.fromProperties(Map.of(
                        Font.KEY_NAME,   HELVETICA,
                        Font.KEY_SIZE,   TEN,
                        "Cartridge",     "9mm Parabellum"
                        )).get());
    }
    
    private static void assertFont(
            final String  expectedName,
            final int     expectedSize,
            final boolean expectedBold,
            final boolean expectedItalic,
            final Font    actual)
    {
        assertEquals(expectedName,   actual.name(),     "name");
        assertEquals(expectedSize,   actual.size(),     "size");
        assertEquals(expectedBold,   actual.isBold(),   "bold");
        assertEquals(expectedItalic, actual.isItalic(), "italic");
    }

}
