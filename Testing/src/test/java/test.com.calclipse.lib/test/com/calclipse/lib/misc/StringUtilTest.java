package test.com.calclipse.lib.misc;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.lib.misc.StringUtil;

@DisplayName("StringUtil")
public final class StringUtilTest
{
    private static final String BOO_AND_FOO = "boo:and:foo";

    public StringUtilTest()
    {
    }
    
    @Test
    @DisplayName("splitUnlessEmpty(String, String, int)")
    public void test1()
    {
        assertArrayEquals(
                new String[0],
                StringUtil.splitUnlessEmpty("", ":", 2),
                "(, :, 2)");
        assertArrayEquals(
                new String[] {"boo", "and:foo"},
                StringUtil.splitUnlessEmpty(BOO_AND_FOO, ":", 2),
                "(boo:and:foo, :, 2)");
        assertArrayEquals(
                new String[] {"b", "", ":and:f", "", ""},
                StringUtil.splitUnlessEmpty(BOO_AND_FOO, "o", -1),
                "(boo:and:foo, o, -1)");
        assertArrayEquals(
                new String[] {"b", "", ":and:f"},
                StringUtil.splitUnlessEmpty(BOO_AND_FOO, "o", 0),
                "(boo:and:foo, o, 0)");
    }
    
    @Test
    @DisplayName("endsWith(CharSequence, CharSequence)")
    public void test2()
    {
        assertTrue(StringUtil.endsWith("", ""), "(, )");
        assertTrue(StringUtil.endsWith("ab", ""), "(ab, )");
        assertFalse(StringUtil.endsWith("", "bc"), "(, bc)");
        assertFalse(StringUtil.endsWith("c", "cd"), "(c, cd)");
        assertFalse(StringUtil.endsWith("d", "cd"), "(d, cd)");
        assertTrue(StringUtil.endsWith("e", "e"), "(e, e)");
        assertFalse(StringUtil.endsWith("f", "g"), "(f, g)");
        assertTrue(StringUtil.endsWith("xyz", "xyz"), "(xyz, xyz)");
        assertTrue(StringUtil.endsWith("xyz", "yz"), "(xyz, yz)");
    }
    
    @Test
    @DisplayName("endsWithPartially(CharSequence, CharSequence)")
    public void test3()
    {
        assertFalse(StringUtil.endsWithPartially("", ""), "(, )");
        assertFalse(StringUtil.endsWithPartially("a", ""), "(a, )");
        assertFalse(StringUtil.endsWithPartially("ab", ""), "(ab, )");
        assertFalse(StringUtil.endsWithPartially("", "c"), "(, c)");
        assertFalse(
                StringUtil.endsWithPartially("te http", "http"),
                "(te http, http)");
        assertTrue(
                StringUtil.endsWithPartially("te htt", "http"),
                "(te htt, http)");
        assertTrue(
                StringUtil.endsWithPartially("te ht", "http"),
                "(te ht, http)");
        assertTrue(
                StringUtil.endsWithPartially("te h", "http"),
                "(te h, http)");
        assertFalse(
                StringUtil.endsWithPartially("te ", "http"),
                "(te , http)");
    }

}
