package test.com.calclipse.lib.arrange;

import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import com.calclipse.lib.arrange.Arrangable;
import com.calclipse.lib.arrange.Arrangement;
import com.calclipse.lib.arrange.Succession;

public final class SuccessionTest
{
    public SuccessionTest()
    {
    }
    
    @Nested
    public static final class
    EMPTY
    {
        @ParameterizedTest
        @CsvSource({"-2", "-1", "0", "1", "2"})
        public void
        insertion_index_of_a_key_is_zero
        (final int key)
        {
            assertEquals(0, empty().insertionIndexOf(key));
        }
        
        @ParameterizedTest
        @CsvSource({"0, 0", "2, 2", "-2, 3"})
        public void
        intervals_are_empty
        (final int fromKey, final int toKey)
        {
            assertTrue(empty().interval(fromKey, toKey).isEmpty());
        }
        
        @ParameterizedTest
        @CsvSource({"0, 0", "2, 2", "-2, 3"})
        public void
        interval_removal_does_nothing_and_gives_empty_result
        (final int fromKey, final int toKey)
        {
            final Arrangement<Arrangable> instance = empty();
            final Collection<Arrangable> result = instance.removeInterval(
                    fromKey, toKey);
            assertTrue(result.isEmpty(),   "result_is_empty");
            assertTrue(instance.isEmpty(), "leaves_instance_empty");
        }
    }
    
    @Nested
    public static final class
    CONTAINING_EXACTLY_ONE_KEY
    {
        @Test
        public void
        a_key_less_than_the_existing_is_inserted_first
        ()
        {
            final Arrangement<Arrangable> instance = keys(2);
            assertEquals(0, instance.insertionIndexOf(1));
            instance.add(() -> 1);
            assertContainsKeys(instance, 1, 2);
            
        }
        
        @Test
        public void
        a_key_equal_to_the_existing_is_inserted_after
        ()
        {
            final Arrangement<Arrangable> instance = keys(2);
            assertEquals(1, instance.insertionIndexOf(2));
            final Arrangable key = () -> 2;
            instance.add(key);
            assertContainsKeys(instance, 2, 2);
            assertNotSame(key, instance.get(0));
            assertSame(   key, instance.get(1));
        }
        
        @Test
        public void
        a_key_greater_than_the_existing_is_inserted_after
        ()
        {
            final Arrangement<Arrangable> instance = keys(2);
            assertEquals(1, instance.insertionIndexOf(3));
            instance.add(() -> 3);
            assertContainsKeys(instance, 2, 3);
        }
        
        @Test
        public void
        an_interval_covering_the_existing_with_margin_includes_it
        ()
        {
            final Arrangement<Arrangable> instance = keys(118);
            assertContainsKeys(instance.interval(0, 454), 118);
        }
        
        @Test
        public void
        a_non_empty_interval_starting_from_the_existing_includes_it
        ()
        {
            final Arrangement<Arrangable> instance = keys(118);
            assertContainsKeys(instance.interval(118, 119), 118);
        }
    }
    
    @Nested
    public static final class
    CONTAINING_EXACTLY_TWO_EQUAL_KEYS
    {
        @Test
        public void
        a_key_less_than_the_existing_is_inserted_before_them
        ()
        {
            final Arrangement<Arrangable> instance = keys(2, 2);
            assertEquals(0, instance.insertionIndexOf(1));
            instance.add(() -> 1);
            assertContainsKeys(instance, 1, 2, 2);
        }
        
        @Test
        public void
        a_key_equal_to_the_existing_is_inserted_after_them
        ()
        {
            final Arrangement<Arrangable> instance = keys(2, 2);
            assertEquals(2, instance.insertionIndexOf(2));
            final Arrangable key = () -> 2;
            instance.add(key);
            assertContainsKeys(instance, 2, 2, 2);
            assertNotSame(key, instance.get(0));
            assertNotSame(key, instance.get(1));
            assertSame(   key, instance.get(2));
        }
        
        @Test
        public void
        a_key_greater_than_the_existing_is_inserted_after_them
        ()
        {
            final Arrangement<Arrangable> instance = keys(2, 2);
            assertEquals(2, instance.insertionIndexOf(3));
            instance.add(() -> 3);
            assertContainsKeys(instance, 2, 2, 3);
        }
    }
    
    @Nested
    public static final class
    CONTAINING_EXACTLY_TWO_UNIQUE_KEYS
    {
        @Test
        public void
        a_key_less_than_both_existing_is_inserted_before_them
        ()
        {
            final Arrangement<Arrangable> instance = keys(2, 3);
            assertEquals(0, instance.insertionIndexOf(1));
            instance.add(() -> 1);
            assertContainsKeys(instance, 1, 2, 3);
        }
        
        @Test
        public void
        a_key_equal_to_the_first_existing_is_inserted_between_them
        ()
        {
            final Arrangement<Arrangable> instance = keys(2, 3);
            assertEquals(1, instance.insertionIndexOf(2));
            final Arrangable key = () -> 2;
            instance.add(key);
            assertContainsKeys(instance, 2, 2, 3);
            assertNotSame(key, instance.get(0));
            assertSame(   key, instance.get(1));
            assertNotSame(key, instance.get(2));
        }
        
        @Test
        public void
        a_key_greater_than_1st_and_less_than_2nd_is_inserted_in_between
        ()
        {
            final Arrangement<Arrangable> instance = keys(2, 4);
            assertEquals(1, instance.insertionIndexOf(3));
            instance.add(() -> 3);
            assertContainsKeys(instance, 2, 3, 4);
        }
        
        @Test
        public void
        a_key_equal_to_the_last_existing_is_inserted_after_both
        ()
        {
            final Arrangement<Arrangable> instance = keys(2, 3);
            assertEquals(2, instance.insertionIndexOf(3));
            final Arrangable key = () -> 3;
            instance.add(key);
            assertContainsKeys(instance, 2, 3, 3);
            assertNotSame(key, instance.get(0));
            assertNotSame(key, instance.get(1));
            assertSame(   key, instance.get(2));
        }
        
        @Test
        public void
        a_key_greater_than_both_existing_is_inserted_after_both
        ()
        {
            final Arrangement<Arrangable> instance = keys(2, 3);
            assertEquals(2, instance.insertionIndexOf(4));
            instance.add(() -> 4);
            assertContainsKeys(instance, 2, 3, 4);
        }
    }
    
    @Nested
    public static final class
    CONTAINING_MULTIPLE_ALL_UNIQUE_KEYS
    {
        @ParameterizedTest
        @CsvSource({
            "-1, 1", "0, 2", "4, 7", "7, 7", "8, 9",
            "10, 10", "11, 11", "12, 12", "12, 121"})
        public void
        removing_non_existing_keys_does_nothing_and_gives_empty_result
        (final int fromKey, final int toKey)
        {
            final Arrangement<Arrangable> instance = keys(2, 3, 7, 11);
            final Collection<Arrangable> result = instance.removeInterval(
                    fromKey, toKey);
            assertTrue(result.isEmpty(), "result_is_empty");
            assertContainsKeys(instance, 2, 3, 7, 11);
        }
        
        @Test
        public void
        interval_includes_fromkey_up_to_excluding_tokey
        ()
        {
            final Arrangement<Arrangable> instance = keys(
                    2, 3, 7, 11, 13, 14, 21, 22, 23, 32, 33);
            assertContainsKeys(instance.interval(-1, 3), 2);
            assertContainsKeys(instance.interval(3, 4), 3);
            assertContainsKeys(instance.interval(6, 19), 7, 11, 13, 14);
            assertContainsKeys(instance.interval(23, 34), 23, 32, 33);
        }
        
        @Test
        public void
        interval_removal_removes_keys_in_interval_and_returns_removed
        ()
        {
            final Arrangement<Arrangable> instance = keys(
                    2, 3, 7, 11, 13, 14, 21, 22, 23, 32, 33);
            final Collection<Arrangable> remove_minus1_to_3
                = instance.removeInterval(-1, 3);
            assertContainsKeys(instance, 3, 7, 11, 13, 14, 21, 22, 23, 32, 33);
            assertContainsKeys(remove_minus1_to_3, 2);
            final Collection<Arrangable> remove_3_to_4
                = instance.removeInterval(3, 4);
            assertContainsKeys(instance, 7, 11, 13, 14, 21, 22, 23, 32, 33);
            assertContainsKeys(remove_3_to_4, 3);
            final Collection<Arrangable> remove_6_to_19
                = instance.removeInterval(6, 19);
            assertContainsKeys(instance, 21, 22, 23, 32, 33);
            assertContainsKeys(remove_6_to_19, 7, 11, 13, 14);
            final Collection<Arrangable> remove_23_to_34
                = instance.removeInterval(23, 34);
            assertContainsKeys(instance, 21, 22);
            assertContainsKeys(remove_23_to_34, 23, 32, 33);
        }
    }
    
    @Nested
    public static final class
    CONTAINING_MULTIPLE_KEYS_WITH_SOME_DUPLICATES
    {
        @Test
        public void
        a_key_less_than_all_is_inserted_first
        ()
        {
            final Arrangement<Arrangable> instance = keys(
                    2, 3, 7, 11, 13, 14, 21, 22, 22, 23, 32, 33, 34);
            assertEquals(0, instance.insertionIndexOf(1));
            instance.add(() -> 1);
            assertContainsKeys(
                    instance,
                    1, 2, 3, 7, 11, 13, 14, 21, 22, 22, 23, 32, 33, 34);
        }
        
        @Test
        public void
        a_key_equal_to_a_unique_key_is_inserted_after_it
        ()
        {
            final Arrangement<Arrangable> instance = keys(
                    2, 3, 7, 11, 13, 14, 21, 22, 22, 23, 32, 33, 34);
            assertEquals(7, instance.insertionIndexOf(21));
            final Arrangable key = () -> 21;
            instance.add(key);
            assertContainsKeys(
                    instance,
                    2, 3, 7, 11, 13, 14, 21, 21, 22, 22, 23, 32, 33, 34);
            assertNotSame(key, instance.get(6));
            assertSame(   key, instance.get(7));
        }
        
        @Test
        public void
        a_key_equal_to_duplicate_keys_is_inserted_after_them
        ()
        {
            final Arrangement<Arrangable> instance = keys(
                    2, 3, 7, 11, 13, 14, 21, 22, 22, 23, 32, 33, 34);
            assertEquals(9, instance.insertionIndexOf(22));
            final Arrangable key = () -> 22;
            instance.add(key);
            assertContainsKeys(
                    instance,
                    2, 3, 7, 11, 13, 14, 21, 22, 22, 22, 23, 32, 33, 34);
            assertNotSame(key, instance.get(7));
            assertNotSame(key, instance.get(8));
            assertSame(   key, instance.get(9));
        }
        
        @Test
        public void
        a_key_equal_to_the_last_key_is_inserted_last
        ()
        {
            final Arrangement<Arrangable> instance = keys(
                    2, 3, 7, 11, 13, 14, 21, 22, 22, 23, 32, 33, 34);
            assertEquals(13, instance.insertionIndexOf(34));
            final Arrangable key = () -> 34;
            instance.add(key);
            assertContainsKeys(
                    instance,
                    2, 3, 7, 11, 13, 14, 21, 22, 22, 23, 32, 33, 34, 34);
            assertNotSame(key, instance.get(12));
            assertSame(   key, instance.get(13));
        }
        
        @Test
        public void
        a_key_greater_than_all_is_inserted_last
        ()
        {
            final Arrangement<Arrangable> instance = keys(
                    2, 3, 7, 11, 13, 14, 21, 22, 22, 23, 32, 33, 34);
            assertEquals(13, instance.insertionIndexOf(44));
            instance.add(() -> 44);
            assertContainsKeys(
                    instance,
                    2, 3, 7, 11, 13, 14, 21, 22, 22, 23, 32, 33, 34, 44);
        }
        
        @Test
        public void
        removing_interval_with_duplicates_gives_duplicates_in_the_result
        ()
        {
            final Arrangement<Arrangable> instance = keys(
                    2, 3, 7, 11, 13, 14, 21, 22, 22, 23, 32, 33, 34);
            final Collection<Arrangable> result = instance.removeInterval(
                    21, 32);
            assertContainsKeys(result, 21, 22, 22, 23);
            assertContainsKeys(instance, 2, 3, 7, 11, 13, 14, 32, 33, 34);
            final var removed = new ArrayList<>(result);
            assertNotSame(removed.get(1), removed.get(2));
        }
    }
    
    private static void assertContainsKeys(
            final Iterable<? extends Arrangable> actualKeys,
            final int... expectedKeys)
    {
        final Collection<Integer> actual = collectKeys(actualKeys);
        final Collection<Integer> expected = collectKeys(expectedKeys);
        assertEquals(
                expected.size(),
                actual.size(),
                "Expected key count differs from actual key count");
        for (final int key : expected)
        {
            assertTrue(actual.contains(key), "Missing key: " + key);
        }
        for (final int key : actual)
        {
            assertTrue(expected.contains(key), "Unexpected key: " + key);
        }
    }

    private static Collection<Integer> collectKeys(final int... keys)
    {
        return IntStream.of(keys).boxed().collect(toList());
    }

    private static Collection<Integer> collectKeys(
            final Iterable<? extends Arrangable> keys)
    {
        return StreamSupport.stream(keys.spliterator(), false)
                .map(Arrangable::key)
                .collect(toList());
    }
    
    private static Arrangement<Arrangable> empty()
    {
        return new Succession<>();
    }

    private static Arrangement<Arrangable> keys(
            final int firstKey, final int... remainingKeys)
    {
        final var instance = new Succession<Arrangable>();
        IntStream.concat(IntStream.of(firstKey), IntStream.of(remainingKeys))
            .forEach(key -> instance.add(() -> key));
        return instance;
    }

}
