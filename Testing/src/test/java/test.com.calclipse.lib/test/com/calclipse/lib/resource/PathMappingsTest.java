package test.com.calclipse.lib.resource;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.io.File;
import java.util.List;
import java.util.function.UnaryOperator;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.lib.resource.PathMappings;

@DisplayName("PathMappings")
public final class PathMappingsTest
{
    public PathMappingsTest()
    {
    }
    
    @Test
    @DisplayName("concatenating(Function, Function)")
    public void test1()
    {
        final var a = new File("a");
        final var b = new File("b");
        final var c = new File("c");
        final UnaryOperator<List<File>> first = components -> List.of(a, b);
        final UnaryOperator<List<File>> second = components -> List.of(c);
        final UnaryOperator<List<File>> both = PathMappings.concatenating(
                first, second);
        final File[] actual = both.apply(List.of()).stream()
                .toArray(File[]::new);
        final File[] expected = new File[] {a, b, c};
        assertArrayEquals(expected, actual, "{a, b} + {c}");
    }

}
