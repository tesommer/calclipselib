package test.com.calclipse.lib.resource;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import com.calclipse.lib.resource.Resource;

/**
 * A mock resource.
 * 
 * @author Tone Sommerland
 */
public final class MockResource implements Resource
{
    private final InputStream[] ins;
    private int index;

    /**
     * Creates a fake resource containing the given input streams.
     */
    public MockResource(final InputStream... ins)
    {
        this.ins = ins.clone();
    }
    
    @Override
    public Optional<InputStream> next() throws IOException
    {
        if (index < ins.length)
        {
            return Optional.of(ins[index++]);
        }
        return Optional.empty();
    }

}
