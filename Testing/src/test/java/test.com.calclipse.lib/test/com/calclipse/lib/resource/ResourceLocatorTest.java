package test.com.calclipse.lib.resource;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.Test;

import com.calclipse.lib.resource.ResourceLocator;

public final class ResourceLocatorTest
{
    public ResourceLocatorTest()
    {
    }

    @Test
    public void
    combining_them_adds_their_capabilities
    () throws IOException
    {
        final var in1 = new ByteArrayInputStream(new byte[0]);
        final var in2 = new ByteArrayInputStream(new byte[0]);
        final var file1 = new File("a.txt");
        final var file2 = new File("b.txt");
        final var file3 = new File("c.txt");
        final ResourceLocator nothing = find_nothing();
        final ResourceLocator in1_on_file1 = find_in_on_file(in1, file1);
        final ResourceLocator in2_on_file2 = find_in_on_file(in2, file2);
        final ResourceLocator in2_on_file1 = find_in_on_file(in2, file1);
        final ResourceLocator combo
            = nothing
                .combinedWith(in1_on_file1)
                .combinedWith(in2_on_file2)
                .combinedWith(in2_on_file1);
        final String resource_name = "/whatever.txt";
        assertSame(in1, combo.find(resource_name, file1), "in1_on_file1");
        assertSame(in2, combo.find(resource_name, file2), "in2_on_file2");
        assertNull(     combo.find(resource_name, file3), "nothing_on_file3");
    }
    
    private static ResourceLocator find_nothing()
    {
        return new MockResourceLocator();
    }
    
    private static ResourceLocator find_in_on_file(
            final InputStream in, final File file)
    {
        return new MockResourceLocator(file, in);
    }

}
