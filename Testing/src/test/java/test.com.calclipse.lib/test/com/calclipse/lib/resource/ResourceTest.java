package test.com.calclipse.lib.resource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Resource")
public final class ResourceTest
{
    private static final InputStream
    IN1 = new ByteArrayInputStream(new byte[0]);
    
    private static final InputStream
    IN2 = new ByteArrayInputStream(new byte[0]);

    public ResourceTest()
    {
    }
    
    @Nested
    @DisplayName("Resource.use(ResourceUser)")
    public static final class UseResourceUserTest
    {
        public UseResourceUserTest()
        {
        }
        
        @Test
        @DisplayName("Empty resource")
        public void test1() throws IOException
        {
            final var sideEffects = new ArrayList<InputStream>(0);
            final var resource = new MockResource();
            resource.use(sideEffects::add);
            assertTrue(sideEffects.isEmpty(), "Assert zero invocations");
        }
        
        @Test
        @DisplayName("All resource files in non-empty resource")
        public void test2() throws IOException
        {
            final var sideEffects = new ArrayList<InputStream>(2);
            final var resource = new MockResource(IN1, IN2);
            resource.use(sideEffects::add);
            assertEquals(
                    2,
                    sideEffects.size(),
                    "Assert one invocation for each resource file");
            assertSame(IN1, sideEffects.get(0), "First file used");
            assertSame(IN2, sideEffects.get(1), "Second file used");
        }
        
        @Test
        @DisplayName("Use and abort")
        public void test3() throws IOException
        {
            final var sideEffects = new ArrayList<InputStream>(2);
            final var resource = new MockResource(IN1, IN2);
            resource.use(in ->
            {
                sideEffects.add(in);
                return false;
            });
            assertEquals(1, sideEffects.size(), "Number of files used");
            assertSame(IN1, sideEffects.get(0), "File used");
        }
        
        @Test
        @DisplayName("Ensure stream is closed automatically")
        public void test4() throws IOException
        {
            final var in1 = new CloseSideEffect();
            final var resource = new MockResource(in1);
            resource.use(in -> true);
            assertTrue(in1.isCloseCalled(), "Close called");
        }
        
    }
    
    private static final class CloseSideEffect extends InputStream
    {
        private boolean closeCalled;

        public CloseSideEffect()
        {
        }

        public boolean isCloseCalled()
        {
            return closeCalled;
        }

        @Override
        public void close() throws IOException
        {
            super.close();
            closeCalled = true;
        }

        @Override
        public int read() throws IOException
        {
            return 0;
        }
        
    }

}
