package test.com.calclipse.lib.misc;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Locale;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.calclipse.lib.misc.EncodingUtil;

@DisplayName("EncodingUtil")
public final class EncodingUtilTest
{
    private static final int MAX_BYTE_VALUE = 256;

    public EncodingUtilTest()
    {
    }
    
    @Nested
    @DisplayName("EncodingUtil.byteToHex(boolean, byte)")
    public static final class ByteToHexBooleanByteTest
    {
        public ByteToHexBooleanByteTest()
        {
        }
        
        @Test
        @DisplayName("Uppercase")
        public void test1()
        {
            final byte[] input = hexBytes();
            final String expected = uppercaseHexString();
            final String actual = byteToHex(true, input);
            assertEquals(expected, actual, "Uppercase result");
        }
        
        @Test
        @DisplayName("Lowercase")
        public void test2()
        {
            final byte[] input = hexBytes();
            final String expected = lowercaseHexString();
            final String actual = byteToHex(false, input);
            assertEquals(expected, actual, "Lowercase result");
        }
    }
    
    @Nested
    @DisplayName("EncodingUtil.bytesToHex(boolean, byte[])")
    public static final class BytesToHexBooleanByteArrayTest
    {
        public BytesToHexBooleanByteArrayTest()
        {
        }
        
        @Test
        @DisplayName("Uppercase")
        public void test1()
        {
            final byte[] input = hexBytes();
            final String expected = uppercaseHexString();
            final String actual = EncodingUtil.bytesToHex(true, input);
            assertEquals(expected, actual, "Uppercase result");
        }
        
        @Test
        @DisplayName("Lowercase")
        public void test2()
        {
            final byte[] input = hexBytes();
            final String expected = lowercaseHexString();
            final String actual = EncodingUtil.bytesToHex(false, input);
            assertEquals(expected, actual, "Lowercase result");
        }
        
        @Test
        @DisplayName("Empty input array")
        public void test3()
        {
            final var input = new byte[0];
            final String actual = EncodingUtil.bytesToHex(false, input);
            assertTrue(actual.isEmpty(), "Assert empty result");
        }
        
    }
    
    @Nested
    @DisplayName("EncodingUtil.hexToByte(char, char)")
    public static final class HexToByteCharCharTest
    {
        public HexToByteCharCharTest()
        {
        }
        
        @Test
        @DisplayName("Uppercase input")
        public void test1()
        {
            final String input = uppercaseHexString();
            final byte[] expected = hexBytes();
            final byte[] actual = hexToByte(input);
            assertArrayEquals(expected, actual, "Result: uppercase input");
        }
        
        @Test
        @DisplayName("Lowercase input")
        public void test2()
        {
            final String input = lowercaseHexString();
            final byte[] expected = hexBytes();
            final byte[] actual = hexToByte(input);
            assertArrayEquals(expected, actual, "Result: lowercase input");
        }
        
        @Test
        @DisplayName("Invalid 1st digit")
        public void test5()
        {
            assertThrows(
                    IllegalArgumentException.class,
                    () -> hexToByte("AX"));
        }
        
        @Test
        @DisplayName("Invalid 2nd digit")
        public void test6()
        {
            assertThrows(
                    IllegalArgumentException.class,
                    () -> hexToByte("TB"));
        }
        
        @Test
        @DisplayName("Lowercase-uppercase combo")
        public void test7()
        {
            final String input = "dDEe";
            final byte[] expected = {(byte)0xdd, (byte)0xee};
            final byte[] actual = hexToByte(input);
            assertArrayEquals(expected, actual, "Result: case mix");
        }
    }
    
    @Nested
    @DisplayName("EncodingUtil.hexToBytes(CharSequence)")
    public static final class HexToBytesCharSequenceTest
    {
        public HexToBytesCharSequenceTest()
        {
        }
        
        @Test
        @DisplayName("Uppercase input")
        public void test1()
        {
            final String input = uppercaseHexString();
            final byte[] expected = hexBytes();
            final byte[] actual = EncodingUtil.hexToBytes(input);
            assertArrayEquals(expected, actual, "Result: uppercase input");
        }
        
        @Test
        @DisplayName("Lowercase input")
        public void test2()
        {
            final String input = lowercaseHexString();
            final byte[] expected = hexBytes();
            final byte[] actual = EncodingUtil.hexToBytes(input);
            assertArrayEquals(expected, actual, "Result: lowercase input");
        }
        
        @Test
        @DisplayName("Empty input")
        public void test3()
        {
            final byte[] actual = EncodingUtil.hexToBytes("");
            assertEquals(0, actual.length, "Result: empty input");
        }
        
        @Test
        @DisplayName("Odd number of input digits")
        public void test4()
        {
            assertThrows(
                    IllegalArgumentException.class,
                    () -> EncodingUtil.hexToBytes("ABC"));
        }
        
        @Test
        @DisplayName("Invalid 1st digit")
        public void test5()
        {
            assertThrows(
                    IllegalArgumentException.class,
                    () -> EncodingUtil.hexToBytes("AX"));
        }
        
        @Test
        @DisplayName("Invalid 2nd digit")
        public void test6()
        {
            assertThrows(
                    IllegalArgumentException.class,
                    () -> EncodingUtil.hexToBytes("TB"));
        }
        
        @Test
        @DisplayName("Lowercase-uppercase combo")
        public void test7()
        {
            final String input = "ddEEfFBb";
            final byte[] expected
                = {(byte)0xdd, (byte)0xee, (byte)0xff, (byte)0xbb};
            final byte[] actual = EncodingUtil.hexToBytes(input);
            assertArrayEquals(expected, actual, "Result: case mix");
        }
    }
    
    private static byte[] hexBytes()
    {
        final var bytes = new byte[MAX_BYTE_VALUE];
        byte b = 0x00;
        for (int i = 0; i < bytes.length; i++)
        {
            bytes[i] = b;
            b += 0x01;
        }
        return bytes;
    }
    
    private static String uppercaseHexString()
    {
        return
              "000102030405060708090A0B0C0D0E0F"
            + "101112131415161718191A1B1C1D1E1F"
            + "202122232425262728292A2B2C2D2E2F"
            + "303132333435363738393A3B3C3D3E3F"
            + "404142434445464748494A4B4C4D4E4F"
            + "505152535455565758595A5B5C5D5E5F"
            + "606162636465666768696A6B6C6D6E6F"
            + "707172737475767778797A7B7C7D7E7F"
            + "808182838485868788898A8B8C8D8E8F"
            + "909192939495969798999A9B9C9D9E9F"
            + "A0A1A2A3A4A5A6A7A8A9AAABACADAEAF"
            + "B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF"
            + "C0C1C2C3C4C5C6C7C8C9CACBCCCDCECF"
            + "D0D1D2D3D4D5D6D7D8D9DADBDCDDDEDF"
            + "E0E1E2E3E4E5E6E7E8E9EAEBECEDEEEF"
            + "F0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF";
    }
    
    private static String lowercaseHexString()
    {
        return uppercaseHexString().toLowerCase(Locale.getDefault());
    }
    
    private static String byteToHex(
            final boolean uppercase, final byte... bytes)
    {
        final var buffer = new StringBuilder(bytes.length * 2);
        for (final byte b : bytes)
        {
            buffer.append(EncodingUtil.byteToHex(uppercase, b));
        }
        return buffer.toString();
    }
    
    private static byte[] hexToByte(final CharSequence hex)
    {
        final var bytes = new byte[hex.length() / 2];
        for (int i = 0; i < hex.length(); i += 2)
        {
            bytes[i / 2] = EncodingUtil.hexToByte(
                    hex.charAt(i), hex.charAt(i + 1));
        }
        return bytes;
    }

}
