package test.com.calclipse.lib.resource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.lib.resource.Resource;
import com.calclipse.lib.resource.ResourceLocator;
import com.calclipse.lib.resource.Resources;
import com.calclipse.lib.resource.SearchPath;

@DisplayName("Resources")
public final class ResourcesTest
{
    public ResourcesTest()
    {
    }
    
    @Test
    @DisplayName("get(String, SearchPath, ResourceLocator)")
    public void test1() throws IOException
    {
        final var fileA = new File("a");
        final var fileB = new File("b");
        final var fileC = new File("c");
        final var inA = new ByteArrayInputStream(new byte[0]);
        final var inB = new ByteArrayInputStream(new byte[0]);
        final ResourceLocator locator0 = new MockResourceLocator();
        final ResourceLocator locatorA = new MockResourceLocator(fileA, inA);
        final ResourceLocator locatorB = new MockResourceLocator(fileB, inB);
        final ResourceLocator combo
            = locatorA
                .combinedWith(locator0)
                .combinedWith(locatorB);
        final Resource resource = Resources.find(
                "", SearchPath.of(List.of(fileA, fileC, fileB)), combo);
        resource.next().ifPresentOrElse(
                in -> assertSame(in, inA, "1st"),
                () -> fail("1st not present"));
        resource.next().ifPresentOrElse(
                in -> assertSame(in, inB, "2nd"),
                () -> fail("2nd not present"));
        assertFalse(resource.next().isPresent(), "Assert no one left");
    }

}
