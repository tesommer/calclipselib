#!/bin/sh

javadoc -d @dirjavadoc@/com.calclipse.lib \
        -sourcepath @dirsrc@/main/java/com.calclipse.lib \
        -subpackages com.calclipse.lib

tar -cvpzf @dirhome@.tar.gz @dirhome@
tar -cvpzf @dirjavadoc@.tar.gz @dirjavadoc@

zip -r @dirhome@.zip @dirhome@
zip -r @dirjavadoc@.zip @dirjavadoc@
