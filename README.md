[CalclipseLib](http://calclipse.com)
============================================

A small library of general usability.

License
-------

See [LICENSE.txt](https://bitbucket.org/tesommer/calclipselib/src/master/LICENSE.txt).

Building
--------

    $ git clone https://bitbucket.org/tesommer/calclipselib.git CalclipseLib
    $ cd CalclipseLib/Building
    $ ant # Creates CalclipseLib/Build

*Copyright (C) 2020 Tone Sommerland*
